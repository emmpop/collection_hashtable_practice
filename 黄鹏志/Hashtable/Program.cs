﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtables
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable ht = new Hashtable();
            while (true)
            {
                Console.WriteLine("\r\n  =======     请选择操作     =======");
                Console.WriteLine("     1、添加图书        2、查找图书");
                Console.WriteLine("  ===================================\r\n");
                Console.WriteLine("请输入你的选择：");
                string f = Console.ReadLine();
                switch (f)
                {
                    case "1":
                        Console.Write("请输入书籍名：");
                        string name = Console.ReadLine();
                        Console.WriteLine("请输入书籍编号：");
                        string tel = Console.ReadLine();
                        if (ht.ContainsKey(name))
                        {
                            Console.WriteLine("该书籍已存在！", "错误！");
                            return;
                        }
                        ht.Add(name, tel);
                        Console.WriteLine($"*****共有{ht.Count}本书籍*****");
                        break;
                    case "2":
                        Console.WriteLine("请输入你要查找的书籍名：");
                        string nameFind = Console.ReadLine();
                        Object telFind = ht[nameFind];
                        if (telFind == null)
                        {
                            Console.WriteLine("该书籍还未添加！请添加或者查看书籍名是否正确");
                        }
                        else
                        {
                            Console.WriteLine($"你所查找的书籍编号是：{telFind.ToString()}");
                        }
                        break;
                }
            }
            }
    }
}
