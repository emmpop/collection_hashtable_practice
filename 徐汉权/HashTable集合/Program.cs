﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashTable集合
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable time = new Hashtable();
            time.Add(1, "人间不值得。很多人对这句话可能有一个误解,");
            time.Add(2, "他们会觉得，人间不值得我来这一趟。");
            time.Add(3, "而这句话本身的意思是积极地：人间本就是这样,");
            time.Add(4, "它不值得你难过，活得洒脱一点。");
            foreach (var a in time.Keys)
            {
                Console.WriteLine("键__:{0}_;_内容__:{1}",a,time[a]);
            }

            //==========================================（分割线）==============================================

            Hashtable cz = new Hashtable();
            cz.Add(1, "一等奖__全国密卷一套");
            cz.Add(2, "二等奖__川师大数学密卷（上下册）一套");
            cz.Add(3, "三等奖__绵阳师大英语密卷（上下册）一套");
            cz.Add(4,"安慰奖__全国密卷一套，川师大数学密卷（上下册）一套，绵阳师大英语密卷（上下册）一套");
            Console.WriteLine("请刮开你手机屏幕前的数字后输入该数字，查询是否中奖。");
            int sum = int.Parse(Console.ReadLine());
            bool sb = cz.ContainsKey(sum);
            if (sb)
            {
                Console.WriteLine("恭喜你中的是:{0}", cz[sum].ToString());
            }
            else {
                Console.WriteLine("请输入正确的数字，领取属于您的奖励！！");
            }
        }
    }
}
