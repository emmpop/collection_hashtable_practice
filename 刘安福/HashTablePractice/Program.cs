﻿using System;
using System.Collections;

namespace HashTablePractice
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable hashtable = new Hashtable();

            hashtable.Add(1, "你轻轻一个吻");
            hashtable.Add("2", "天青色等烟雨");
            hashtable.Add(3, "我的故事就是你啊");
            hashtable.Add(4, "说好带你流浪");

            foreach(DictionaryEntry item in hashtable)
            {
                var k = item.Key;
                var v = item.Value;

                Console.WriteLine("{0}`{1}",k,v);
            }

            hashtable.Clear();

            Console.WriteLine("-----------");

            hashtable.Add(1, "你轻轻一个吻");
            hashtable.Add("2", "天青色等烟雨");
            hashtable.Add(3, "我的故事就是你啊");
            hashtable.Add(4, "说好带你流浪");

            Console.WriteLine("输入1-4中的一个数字");
            int id = int.Parse(Console.ReadLine());

            if (hashtable.ContainsKey(id))
            {
                Console.WriteLine("{0}",hashtable[id].ToString());
            }
            else
            {
                Console.WriteLine("叫你输1-4 ，你看看你输的是啥玩意儿");
            }

            Console.WriteLine("-----------");
            Console.WriteLine();
            //第三精神病院开院啦

            SortedList sortedList = new SortedList();

            //添加患者信息
            sortedList.Add(1,"：卢全");
            sortedList.Add(2,"：蒙弟");
            sortedList.Add(3,"：朱德");

            Console.WriteLine("以下是我院患者名单");
            foreach(DictionaryEntry item in sortedList)
            {
                var k = item.Key;
                var v = item.Value;

                Console.WriteLine("{0}{1}",k,v);
            }
            Console.WriteLine();
            Console.WriteLine("请输入要查询的患者的编号");

            int _id = int.Parse(Console.ReadLine());

            if (sortedList.ContainsKey(_id))
            {
                Console.WriteLine("这是我第三精神病院的{0}",sortedList[_id].ToString());
            }
            else
            {
                Console.WriteLine("黑纸白字，写的好好的就 1,2,3 你看看你输的是啥玩意 ");
            }
            
        }
    }
}
