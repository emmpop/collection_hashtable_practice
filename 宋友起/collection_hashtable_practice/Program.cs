﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace collection_hashtable_practice
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable students = new Hashtable();
            students.Add(0001, "张三");
            students.Add(0002, "李四");
            students.Add(0003, "王五");
            students.Add(0004, "宋友起");
           
            Console.WriteLine("学生：");
            foreach(DictionaryEntry item in students)
            {
                int id = (int)item.Key;
                string name = (string)item.Value;
                Console.WriteLine("学号：{0}，名字：{1}",id,name);
            }
            students.Remove(0002);
            Console.WriteLine("开除李四");
            foreach (DictionaryEntry item in students)
            {

                int id = (int)item.Key;
                string name = (string)item.Value;
                Console.WriteLine("学号：{0}，名字：{1}", id, name);
            }
                
            while (true)
            {
                Console.WriteLine("是否继续查询(y/n)");
                string a = Console.ReadLine();

                if (a == "y")
                {
                    int id = Convert.ToInt32(Console.ReadLine());
                    bool b = students.ContainsKey(id);
                    if (b==true)
                    {
                        Console.WriteLine("学生：{0}，学号：{1}", students[id].ToString(),id);
                    }
                    else
                    {
                        Console.WriteLine("该学生不存在");
                       
                    }
                }
                else
                {
                    break;

                }
            }
        }
    }
}
