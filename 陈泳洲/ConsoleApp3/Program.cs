﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            //输入朝代  显示对应的第一个君主 
            Console.WriteLine("可输入：夏，商，周，秦，汉，晋，隋，唐，宋，元，明，清");       // 提示用户输入
            string ChaoDai = "夏,商,周,秦,汉,晋,隋,唐,宋,元,明,清";                               //创建朝代字符串
            string JZ = "大禹，武汤，姬发，嬴政，刘邦，司马炎，杨坚，李渊，赵匡胤，忽必烈，朱元璋，爱新觉罗·努尔哈赤";   //创建君主字符串
            
            string[] NewChaoDai = ChaoDai.Split(',');       //分割字符串 用数组保存
            string[] NewJZ = JZ.Split('，');

            Hashtable ht = new Hashtable();             //实例化集合

            for (int i = 0; i < NewJZ.Length; i++)      //添加键值对
            {
                ht.Add(NewChaoDai[i], NewJZ[i]);
            }
            string input = Console.ReadLine();          //用户输入框

 
                if (NewChaoDai.Contains(input))     //判断是否存在键
                {
                    Console.WriteLine("第一个君主是{0}", ht[input]);
                 
                }
                else
                {
                    Console.WriteLine("你的输入有误");
                 
                }
            

            Console.ReadKey();
        }
    }
}
