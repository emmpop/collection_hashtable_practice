﻿using System;
using System.Collections;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable da = new Hashtable();
            da.Add("螺旋烤全牛", 8888);
            da.Add("劲爆天糖", 520);
            da.Add("清水白菜", 4000);
            da.Add("宫保鸡丁", 70);
            da.Add("佛跳墙", 2000);
            da.Add("撒尿牛丸", 20);
            da.Add("炒粉", 10);
            Console.WriteLine("请点菜,菜谱如下。");
            Console.WriteLine("螺旋烤全牛");
            Console.WriteLine("劲爆天糖");
            Console.WriteLine("清水白菜");
            Console.WriteLine("宫保鸡丁");
            Console.WriteLine("佛跳墙");
            Console.WriteLine("撒尿牛丸");
            Console.WriteLine("炒粉");
            Console.WriteLine();
            string foodName = Console.ReadLine();
            bool eat = da.ContainsKey(foodName);
            if (eat)
            {
                Console.WriteLine("好的，即将为您献上{0}一份，价格{1}元，请享用",foodName,da[foodName].ToString());
            }
            else
            {
                Console.WriteLine("先生，请认真看菜谱好嘛！");
            }
        }
    }
}
