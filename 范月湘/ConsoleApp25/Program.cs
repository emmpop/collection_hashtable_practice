﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp25
{
    class Program
    {
        static void Main(string[] args)
        {
            //创建一个键值对集合对象
            Hashtable ht = new Hashtable();
            //Add添加
            ht.Add(1, "(´っω•｀。)");
            ht.Add(2, "(*´▽｀)ノノ");
            ht.Add(3, "(*╹▽╹*)");
            ht.Add(4, "(づ｡◕‿‿◕｡)づ");
            ht.Add("小表情", new int[] { 5, 6, 7 });
            ht[4] = "路过";//也是添加数据的方式
            ht[1] = "揉眼";
            //ht.Clear();
            //ht.Remove(1);
            //Contains与ContainsKey是一样的
            bool b = ht.Contains(1);
            bool c = ht.ContainsKey(1);
            bool d = ht.ContainsValue("(´っω•｀。)");//检查是否包含值为"(´っω•｀。)"的键值对

            Hashtable hq = new Hashtable();
            hq.Add(1, "(´っω•｀。)");
            hq.Add(2, "(*´▽｀)ノノ");
            for (int i = 0; i < hq.Count; i++)
            {
                Console.WriteLine(hq[i]);
            }
            foreach (var item in ht.Keys)
            {
                Console.WriteLine("键是{0}，值是{1}，item,hq[item]");
            }
            foreach (var item in hq.Values)
            {
                Console.WriteLine(item);
            }
            Console.ReadKey();

        }
    }
}
