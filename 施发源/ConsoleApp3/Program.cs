﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {

            Hashtable ht = new Hashtable();
            ht.Add(1, "张三");
            ht.Add(2, "李四");
            ht.Add(3, "王五");
            Console.WriteLine("请输入学生学号");
            int id = int.Parse(Console.ReadLine());
            
            if (ht.ContainsKey(id))
            {
                Console.WriteLine($"您查找的学生姓名为：{ht[id]}");
            }
            else
            {
                Console.WriteLine("您查找的图书编号不存在！");
            }
            Console.WriteLine("所有的学生信息如下：");
            foreach (DictionaryEntry d in ht)
            {
                int key = (int)d.Key;
                string value = d.Value.ToString();
                Console.WriteLine($"学号：{key}，学生姓名：{value}");
            }

        }
    }
}
