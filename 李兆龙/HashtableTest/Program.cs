﻿using System;
using System.Collections;

namespace HashtableTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable hashtable = new Hashtable();

            hashtable.Add("X", "x");
            hashtable.Add("V", "v");
            hashtable.Add("Z", "z");
            hashtable.Add("A", "a");

            if (hashtable.Contains("y"))//判断哈希表是否包含特定键
            {
                Console.WriteLine("The y key:exist.");
            }
            else
            {
                Console.WriteLine("The y key:not exist");
            }
             
            //遍历哈希表
            foreach(DictionaryEntry item in hashtable)
            {
                Console.WriteLine(item.Key);//item.Key对应于key/键值对key 
                Console.WriteLine(item.Value);//item.Value对应于key/键值对
            }


            //对哈希表进行排序

            ArrayList arrList = new ArrayList(hashtable.Keys);
            arrList.Sort();//按字母顺序进行排序
            foreach(string aka in arrList)
            {
                Console.WriteLine(hashtable[aka]);//排序后输出
            }
        }
    }
}
