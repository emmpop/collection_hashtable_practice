﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace DemoHashTable
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable ht = new Hashtable();
            ht.Add(1, "一是婴儿哭啼");
            ht.Add(2, "二是学游戏");
            ht.Add(3, "三是青春物语");
            ht.Add(4, "四是碰巧遇见你");


          
            Console.WriteLine("请输入一个数字");
            int num = int.Parse(Console.ReadLine());
            bool a = ht.ContainsKey(num); //判断
            if (a)
            {
                Console.WriteLine("对应歌词为:{0}\n", ht[num].ToString());
            }
            else
            {
                Console.WriteLine("找到歌词\n");
            }
           

            Console.WriteLine("年轮说");
           // 实例化一个ArrayList对象 并把ht的keys进行赋值
            ArrayList list = new ArrayList(ht.Keys);

            list.Sort();  //正序排序

            foreach (var  key in list)
            {
              
                Console.WriteLine(ht[key]); //排序后输出
            }

            Console.WriteLine();
            foreach (DictionaryEntry item in ht)
            {
                int key = (int)item.Key;
                string value = item.Value.ToString();
                Console.WriteLine("歌词第{0}句,为：{1}", key, value);
            }

            // 键值对KeyValuePair < T，K > 遍历
            //foreach (KeyValuePair<int, string> item in ht)
            //{
            //    Console.WriteLine(item.Key + item.Value);
            //}

        }
    }
}
