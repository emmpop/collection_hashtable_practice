﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable library = new Hashtable { { 1, "水浒传" }, { 2, "爆笑校园" }, { 3, "一千零一夜" } };
            Console.WriteLine("请输入图书编号");
            int id = int.Parse(Console.ReadLine());
            bool flag = library.ContainsKey(id);
            if (flag)
            {
                Console.WriteLine("你寻找的图书是:{0}", library[id].ToString());
            }
            else
            {
                Console.WriteLine("你寻找的图书不存在");
            }
            Console.WriteLine("所有的图书信息如下");
            foreach (DictionaryEntry a in library)
            {
                int key = (int)a.Key;
                string value = a.Value.ToString();
                Console.WriteLine("图书编号：{0}，图书名称：{1}", key, value);
            }
        }
    }
}
