﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashTableTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable zd = new Hashtable();
            zd.Add(1, "RNG");
            zd.Add(2, "IG");
            zd.Add(3, "FPX");
            Console.WriteLine("请输入战队名称");
            int id = int.Parse(Console.ReadLine());
            bool flag = zd.ContainsKey(id);
            if (flag)
            {
                Console.WriteLine("你想加入的战队为：{0}",
                zd[id].ToString());
            }
            else
            {
                Console.WriteLine("你要加入的战队不存在");
            }
            Console.WriteLine("你能加入的战队有：");
            foreach (DictionaryEntry d in zd)
            {
                int key = (int)d.Key; string value = d.Value.ToString();
                Console.WriteLine("战队排名：{0}，战队名称：{1}", key, value);
            }
        }
    }
}