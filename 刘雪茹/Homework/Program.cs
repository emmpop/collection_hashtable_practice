﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable myHT = new Hashtable
            {
                { 0, "小红" },
                { 1, "小明" },
                { 2, "李华" },
                { 3, "大杨" },
                { 4, "小赵" },
                { 5, "小陈" },
                { 6, "小朱" }
            };
            Console.WriteLine("请输入学生编号");
            int id = int.Parse(Console.ReadLine());
            bool name = myHT.ContainsKey(id);
            if (name)
            {
                Console.WriteLine("您查找的学生名字为：{0}", myHT[id].ToString());
            }
            else
            {
                Console.WriteLine("您查找的学生编号不存在！");
            }
            Console.WriteLine("所有的学生信息如下：");
            foreach (DictionaryEntry information in myHT)
            {
                int key = (int)information.Key;
                string value = information.Value.ToString();
                Console.WriteLine("学生编号：{0}，姓名：{1}", key, value);
            }
        }
    }
}
