﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable ht = new Hashtable();

            //向集合中添加元素
            ht.Add(1, "天堂");
            ht.Add(2, "人间");
            ht.Add(4, "地狱");

            //判断集合中是否包含指定 key 值的元素
            if (ht.ContainsKey(3))
            {
                Console.WriteLine("存在key为3");
            }
            else
            {
                Console.WriteLine("不存在key3");
            }

            //判断集合中是否包含指定 value 值的元素
            if (ht.ContainsValue("天堂"))
            {
                Console.WriteLine("存在名为天堂的值");
            }
            else
            {
                Console.WriteLine("不存在名为天堂的值");
            }

            //DictionaryEntry可以用于检索键或值对
            foreach (DictionaryEntry htco in ht)
            {
                int key = (int)htco.Key;
                string value = htco.Value.ToString();
                Console.WriteLine("key为：{0}，values为：{1}",key,value);
            }
        }
    }
}
