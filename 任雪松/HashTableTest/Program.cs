﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashTableTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable ht = new Hashtable(); 
            ht.Add(1, "影流之主"); 
            ht.Add(2, "潮汐海灵"); 
            ht.Add(3, "卡牌大师"); 
            Console.WriteLine("请输入英雄编号"); 
            int id = int.Parse(Console.ReadLine());
            bool flag = ht.ContainsKey(id); 
            if (flag)
            {
                Console.WriteLine("您这局选择的英雄为：{0}", 
                ht[id].ToString()); 
            } 
            else 
            { 
                Console.WriteLine("您要选择的英雄不存在"); 
            }
            Console.WriteLine("所有的英雄为："); 
            foreach (DictionaryEntry d in ht) 
            { 
                int key = (int)d.Key; string value = d.Value.ToString(); 
                Console.WriteLine("英雄编号：{0}，英雄名称：{1}", key, value); 
            }
        }
    }
}
