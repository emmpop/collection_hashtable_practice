﻿using System;
using System.Collections;

namespace HAshTable
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("欢迎光临，这是本店菜单");
            Hashtable Hashtable = new Hashtable();
            Hashtable.Add(1,"马铃薯炒土豆");
            Hashtable.Add(2,"西红柿炒鸡蛋");
            Hashtable.Add(3,"大白菜加紫菜汤");
            Hashtable.Add(4,"奥特曼烧酒一箱");
            Hashtable.Add(5,"史前大烤鱼");

            Console.WriteLine("请点餐：");

            int id = int.Parse(Console.ReadLine());
            bool a =Hashtable.ContainsKey(id);

         
            if (a)
            {
                Console.WriteLine("你的点餐：{0}",Hashtable[id].ToString() );
            }
            else
            {
                Console.WriteLine("抱歉，新物品引进中。");
            }

            Console.WriteLine("菜谱明码标价，请放心购买");
            Console.WriteLine("1号￥=30");
            Console.WriteLine("2号￥=25");
            Console.WriteLine("3号￥=2500000？");
            Console.WriteLine("4号￥=9999999？");
            Console.WriteLine("九折水平，本店即将引进新物种，敬请期待。");
        }
    }
}
