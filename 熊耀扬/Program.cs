﻿using System;
using System.Collections;
using System.Reflection.PortableExecutable;

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable hashtable = new Hashtable();
            hashtable.Add("1","金");
            hashtable.Add("2","木");
            hashtable.Add("3","水");
            hashtable.Add("4","火");
            hashtable.Add("5","土");
            //遍历
            foreach (DictionaryEntry item in hashtable)
            {
                Console.WriteLine(item.Key);
                Console.WriteLine(item.Value);
            }
            //移除键值对
            hashtable.Remove("2");
            //判断
            bool s = hashtable.ContainsKey("7");
            Console.WriteLine(s);
            bool i = hashtable.ContainsValue("火");
            Console.WriteLine(i);
            //排序输出
            ArrayList arrayList = new ArrayList(hashtable.Keys);
            arrayList.Sort();
            foreach (var item in arrayList)
            {
                Console.WriteLine(item);
                Console.WriteLine(hashtable[item]);
            }
            //清空集合
            hashtable.Clear();
        }
    }
}
