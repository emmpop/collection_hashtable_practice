﻿using System;
using System.Collections;

namespace Shopping
{
    class Program
    {
        static void Main(string[] args)
        {
            #region 先生进来玩玩么？
            Program program = new Program();
            Hashtable gift = program.gift();
            Hashtable prise = program.price();
            Console.WriteLine("先生您好，欢迎光临本店，请问您需要些什么，把编号告诉我就行；");
            foreach (DictionaryEntry d in gift)
            {
                int key = (int)d.Key;
                string value = d.Value.ToString();
                Console.WriteLine("商品编号：{0}，商品名称：{1}", key, value);
            }
            bool judge = true;
            while (judge)
            {
                int choice =int.Parse( Console.ReadLine());
                bool o = gift.ContainsKey(choice);
                if (o)
                {
                    Console.WriteLine("您要的商品是{0}，价格为{1}。请先生这边付钱", gift[choice].ToString(), prise[choice].ToString());
                    break;
                }
                else
                {
                    Console.WriteLine("您所说的商品不存在，请重新选择");

                }
            }
            #endregion
        }
        #region 商品
        private Hashtable gift()
        {
            Hashtable hashtable = new Hashtable();
            hashtable.Add(1, "项链");
            hashtable.Add(2, "手环");
            hashtable.Add(3, "手镯");
            hashtable.Add(4, "耳环");
            hashtable.Add(5, "耳钉");
            hashtable.Add(6, "头饰");
            hashtable.Add(7, "戒指");
            hashtable.Add(8, "鼻环");
            hashtable.Add(9, "颈环");
            hashtable.Add(10, "脚环");
            return hashtable;
        }
        #endregion

        #region 价格     
        private Hashtable price()
        {
            Hashtable hashtable = new Hashtable();
            hashtable.Add(1, "100块钱");
            hashtable.Add(2, "50块钱");
            hashtable.Add(3, "200块钱");
            hashtable.Add(4, "20块钱");
            hashtable.Add(5, "10块钱");
            hashtable.Add(6, "300块钱");
            hashtable.Add(7, "2万块钱");
            hashtable.Add(8, "15块钱");
            hashtable.Add(9, "30块钱");
            hashtable.Add(10, "50块钱");
            return hashtable;
        }
        #endregion
    }


}
