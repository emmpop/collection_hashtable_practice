﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp23
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable ht = new Hashtable();
            ht.Add(1,"水浒传");
            ht.Add(2, "西游记");
            ht.Add(3, "红楼梦");
            ht.Add(4, "三国演义");
            Console.WriteLine("请输入您查找书籍编号：");
            int id = int.Parse(Console.ReadLine());
            bool flag = ht.ContainsKey(id);
            if (flag)
            {
                Console.WriteLine("您查找的书籍名称为：{0}",ht [id].ToString ());
            }
            else
            {
                Console.WriteLine("对不起，该编号不存在！请确认后输入");
            }
            Console.WriteLine("所有书籍信息如下：");
            foreach (DictionaryEntry b in ht)
            {
                int key = (int)b.Key;
                string value = b.Value.ToString();
                Console.WriteLine("书籍编号：{0}，书籍名称：{1}",key ,value );
            }
        }
    }
}
