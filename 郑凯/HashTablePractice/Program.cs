﻿using System;
using System.Collections;

namespace HashTablePractice
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable ht = new Hashtable();
            ht.Add(1, "武将个人介绍");
            ht.Add(2, "武将搭配");
            ht.Add(3, "武将缘分");
            Console.WriteLine("请输入想要查询编号");
            int id = int.Parse(Console.ReadLine());
            bool flag = ht.ContainsKey(id);
            if (flag)
            {
                Console.WriteLine("您查找的信息为：{0}", ht[id].ToString());
            }
            else
            {
                Console.WriteLine("查询不存在！");
            }
            Console.WriteLine("查询列表如下：");
            foreach (DictionaryEntry d in ht)
            {
                int key = (int)d.Key;
                string value = d.Value.ToString();
                Console.WriteLine("编号：{0}，信息：{1}", key, value);
            }
        }
    }
}
