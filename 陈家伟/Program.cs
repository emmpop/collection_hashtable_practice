﻿using System;
using System.Collections;
namespace CollectionsApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable ht = new Hashtable();


            ht.Add("1","China");
            ht.Add("2","American");
            ht.Add("3","Japan");
            ht.Add("4","London");
            ht.Add("5","Australia");
            ht.Add("6","Thailand");
            ht.Add("7","Singapore");

            if (ht.ContainsValue("Vietnam"))
            {
                Console.WriteLine("This national name is already in the list");
            }
            else
            {
                ht.Add("8", "Vietnam");
            }
            
            ICollection key = ht.Keys;

            foreach (string i in key)
            {
                Console.WriteLine(i + ": " + ht[i]);
            }
            Console.ReadKey();
        }
    }
}
