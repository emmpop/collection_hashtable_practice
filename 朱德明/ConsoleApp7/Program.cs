﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable ht = new Hashtable();
            ht.Add(1, "早上好");
            ht.Add(2, "中午好");
            ht.Add(3, "晚上好");
            ht.Add(4, "我不好");
            Console.WriteLine("请输入1-4随机数字");
            int id = int.Parse(Console.ReadLine());
            bool flag = ht.ContainsKey(id);
            if (flag)
            {
                Console.WriteLine("你得到的祝福语是{0}",ht[id].ToString());
            }
            else
            {
                Console.WriteLine("你没有得到祝福语");
            }
            Console.WriteLine("所有的祝福语情况如下");
            foreach(DictionaryEntry d in ht)
            {
                int key = (int)d.Key;
                string value = d.Value.ToString();
                Console.WriteLine("祝福语编号{0},祝福语内容{1}",key,value);
            }
        }
    }
}
