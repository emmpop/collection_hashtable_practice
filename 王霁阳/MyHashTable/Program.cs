﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace MyHashTable
{
    class Program
    {
        static void Main(string[] args)
        {
            //练习1 先向 Hashtable 集合中添加 3 个值，再根据所输入的 key 值查找图书名称，最后遍历所有的图书信息，
            //实例化集合并且添加几个值
            Hashtable list = new Hashtable();
            list.Add(1, "计算机应用技术");
            list.Add(2, "计算机软件技术");
            list.Add(3, "c#的入门到入土");
            list.Add(4, "java的入门到放弃");
            //根据key查找图书名称
            Console.WriteLine("请输入要查找的书本的id");
            int id = int.Parse(Console.ReadLine());
            if (list.ContainsKey(id))
            {
                Console.WriteLine("您查找的书为:  {0}",list[id]);
            }
            else
            {
                Console.WriteLine("您查找的书不纯在");
            }
            //遍历所有的图书信息
            Console.WriteLine("所有书本信息如下");
            foreach (DictionaryEntry item in list)
            {
                var key = item.Key;
                var vable = item.Value;
                Console.WriteLine("{0}{1}",key,vable);
            }

        }
    }
}
